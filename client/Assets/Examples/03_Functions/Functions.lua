---
--- LuaComponent组件 功能选项
---

local Functions = class("Functions", require("unity.LuaMonoBehaviour"))

---
--- UpdateText LateUpdateText FixedUpdateText
---
function Functions:Awake()
    self.mUpdateCount = 0
    self.mLateUpdateCount = 0
    self.mFixedUpdateCount = 0
end

function Functions:Update()
    self.mUpdateCount = self.mUpdateCount + 1
    self.UpdateText.text = tostring(self.mUpdateCount)
end

function Functions:LateUpdate()
    self.mLateUpdateCount = self.mLateUpdateCount + 1
    self.LateUpdateText.text = tostring(self.mLateUpdateCount)
end

function Functions:FixedUpdate()
    self.mFixedUpdateCount = self.mFixedUpdateCount + 1
    self.FixedUpdateText.text = tostring(self.mFixedUpdateCount)
end

return Functions
