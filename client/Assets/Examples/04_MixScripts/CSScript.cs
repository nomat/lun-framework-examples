﻿using UnityEngine;
using System.Collections.Generic;
using LuaInterface;
using UnityEngine.UI;
using System;

public class CSScript : MonoBehaviour
{
    public LuaMonoBehaviour LuaScript;
    public Text CalledText;
    private int CalledCount = 0;

    public void Start()
    {
        // 向Lua中注册委托函数
        LuaScript.SetVariable<Delegate>("mCallCSharp", new Action(OnCall));
        LuaScript.FreeVariablesCache();
    }

    public void CallLua()
    {
        LuaScript.CallSelf("OnCall");
    }

    public void OnCall()
    {
        CalledCount++;
        CalledText.text = "" + CalledCount;
    }
}
