---
--- 混合C# 与 lua 相互调用
--- Lua组件调用C#组件有两种方式
--- 1、底层通过tolua绑定C#组件类型，然后把C#组件对象传入Lua组件
--- 2、把C#组件回调函数注册到Lua组件变量中
---

local LuaScript = class("LuaScript", require("unity.LuaMonoBehaviour"))

---
--- CalledText CSScript 
--- CSDelegate 表示是否使用回调函数
---
function LuaScript:Awake()
    self.CalledCount = 0
end

function LuaScript:CallCSharp()
    if self.CSDelegate then
        print("第二种方式")
        self.mCallCSharp()
    else
        print("第一种方式")
        self.CSScript:OnCall()
    end
end

function LuaScript:OnCall()
    self.CalledCount = self.CalledCount + 1
    self.CalledText.text = tostring(self.CalledCount)
end

return LuaScript
