---
--- 动态加载资源
---
---

local AssetManager = require("unity.AssetManager")

local AssetLoad = class("AssetLoad", require("unity.LuaMonoBehaviour"))

---
--- PrefabPaths [string]
---
function AssetLoad:Awake()
    
end

function AssetLoad:Start()
    coroutine.start(function()
        for i,a in ipairs(AssetManager:LoadAssetsAsync(self.PrefabPaths)) do
            UnityEngine.Object.Instantiate(a, self.base.transform)
        end
    end)
end

return AssetLoad
