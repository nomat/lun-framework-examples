---
--- 经典 HelloWorld
---

local HelloWorld = class("HelloWorld", require("unity.LuaMonoBehaviour"))

function HelloWorld:Start()
    self.Text.text = "Hello World"
end

return HelloWorld
