---
--- Lua脚本 绑定Unity对象，或者值
---

local BindVariables = class("BindVariables", require("unity.LuaMonoBehaviour"))

function BindVariables:Start()
    self.StringLabel.text = self.String
    self.BooleanLabel.text = tostring(self.Boolean) 
    self.NumberLabel.text = tostring(self.Number)
    self.ColorLabel.color = self.Color
    self.ColorLabel.text = tostring(self.Color)
    self.Vector2Label.text = tostring(self.Vector2)
    self.Vector3Label.text = tostring(self.Vector3)
    self.Vector4Label.text = tostring(self.Vector4)
    self.ResourceImage.sprite = self.Resource
    self.GameObject:SetActive(true)
    self.LuaTest:Show()
end

return BindVariables
