---
--- LuaComponenet 绑定到其他LuaComponenet上， 并测试调用
---

local LuaTest = class("LuaTest", require("unity.LuaMonoBehaviour"))

function LuaTest:Show()
    self.Value:SetActive(true)
end

return LuaTest
