using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

using BindType = LunLuaEditor.ToLuaMenu.BindType;
using System.Reflection;

namespace LunLuaEditor
{

    public static partial class ToLuaSettings
    {
        static ToLuaSettings()
        {
            customTypeList.Add(_GT(typeof(WWW)));
            customTypeList.Add(_GT(typeof(CSScript)));
        }
    }

}
