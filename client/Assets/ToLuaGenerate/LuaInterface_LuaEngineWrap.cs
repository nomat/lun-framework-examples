﻿//this source code was auto-generated by tolua#, do not modify it
using System;
using LuaInterface;

public class LuaInterface_LuaEngineWrap
{
	public static void Register(LuaState L)
	{
		L.BeginClass(typeof(LuaInterface.LuaEngine), typeof(UnityEngine.MonoBehaviour));
		L.RegFunction("Slave", Slave);
		L.RegFunction("ResetEngines", ResetEngines);
		L.RegFunction("Init", Init);
		L.RegFunction("AttachProfiler", AttachProfiler);
		L.RegFunction("DetachProfiler", DetachProfiler);
		L.RegFunction("IsKindOf", IsKindOf);
		L.RegFunction("NewLuaColor", NewLuaColor);
		L.RegFunction("NewLuaVector2", NewLuaVector2);
		L.RegFunction("NewLuaVector3", NewLuaVector3);
		L.RegFunction("NewLuaVector4", NewLuaVector4);
		L.RegFunction("NewLuaLayerMask", NewLuaLayerMask);
		L.RegFunction("__eq", op_Equality);
		L.RegFunction("__tostring", ToLua.op_ToString);
		L.RegVar("GAMEOBJECT", get_GAMEOBJECT, null);
		L.RegVar("Master", get_Master, null);
		L.RegVar("State", get_State, null);
		L.EndClass();
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int Slave(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 1);
			string arg0 = ToLua.CheckString(L, 1);
			LuaInterface.LuaEngine o = LuaInterface.LuaEngine.Slave(arg0);
			ToLua.Push(L, o);
			return 1;
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int ResetEngines(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 0);
			LuaInterface.LuaEngine.ResetEngines();
			return 0;
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int Init(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 1);
			LuaInterface.LuaEngine obj = (LuaInterface.LuaEngine)ToLua.CheckObject<LuaInterface.LuaEngine>(L, 1);
			obj.Init();
			return 0;
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int AttachProfiler(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 1);
			LuaInterface.LuaEngine obj = (LuaInterface.LuaEngine)ToLua.CheckObject<LuaInterface.LuaEngine>(L, 1);
			obj.AttachProfiler();
			return 0;
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int DetachProfiler(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 1);
			LuaInterface.LuaEngine obj = (LuaInterface.LuaEngine)ToLua.CheckObject<LuaInterface.LuaEngine>(L, 1);
			obj.DetachProfiler();
			return 0;
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int IsKindOf(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 3);
			LuaInterface.LuaEngine obj = (LuaInterface.LuaEngine)ToLua.CheckObject<LuaInterface.LuaEngine>(L, 1);
			LuaTable arg0 = ToLua.CheckLuaTable(L, 2);
			string arg1 = ToLua.CheckString(L, 3);
			bool o = obj.IsKindOf(arg0, arg1);
			LuaDLL.lua_pushboolean(L, o);
			return 1;
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int NewLuaColor(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 2);
			LuaInterface.LuaEngine obj = (LuaInterface.LuaEngine)ToLua.CheckObject<LuaInterface.LuaEngine>(L, 1);
			UnityEngine.Color arg0 = ToLua.ToColor(L, 2);
			LuaInterface.LuaTable o = obj.NewLuaColor(arg0);
			ToLua.Push(L, o);
			return 1;
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int NewLuaVector2(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 2);
			LuaInterface.LuaEngine obj = (LuaInterface.LuaEngine)ToLua.CheckObject<LuaInterface.LuaEngine>(L, 1);
			UnityEngine.Vector2 arg0 = ToLua.ToVector2(L, 2);
			LuaInterface.LuaTable o = obj.NewLuaVector2(arg0);
			ToLua.Push(L, o);
			return 1;
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int NewLuaVector3(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 2);
			LuaInterface.LuaEngine obj = (LuaInterface.LuaEngine)ToLua.CheckObject<LuaInterface.LuaEngine>(L, 1);
			UnityEngine.Vector3 arg0 = ToLua.ToVector3(L, 2);
			LuaInterface.LuaTable o = obj.NewLuaVector3(arg0);
			ToLua.Push(L, o);
			return 1;
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int NewLuaVector4(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 2);
			LuaInterface.LuaEngine obj = (LuaInterface.LuaEngine)ToLua.CheckObject<LuaInterface.LuaEngine>(L, 1);
			UnityEngine.Vector4 arg0 = ToLua.ToVector4(L, 2);
			LuaInterface.LuaTable o = obj.NewLuaVector4(arg0);
			ToLua.Push(L, o);
			return 1;
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int NewLuaLayerMask(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 2);
			LuaInterface.LuaEngine obj = (LuaInterface.LuaEngine)ToLua.CheckObject<LuaInterface.LuaEngine>(L, 1);
			UnityEngine.LayerMask arg0 = ToLua.ToLayerMask(L, 2);
			LuaInterface.LuaTable o = obj.NewLuaLayerMask(arg0);
			ToLua.Push(L, o);
			return 1;
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int op_Equality(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 2);
			UnityEngine.Object arg0 = (UnityEngine.Object)ToLua.ToObject(L, 1);
			UnityEngine.Object arg1 = (UnityEngine.Object)ToLua.ToObject(L, 2);
			bool o = arg0 == arg1;
			LuaDLL.lua_pushboolean(L, o);
			return 1;
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int get_GAMEOBJECT(IntPtr L)
	{
		try
		{
			LuaDLL.lua_pushstring(L, LuaInterface.LuaEngine.GAMEOBJECT);
			return 1;
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int get_Master(IntPtr L)
	{
		try
		{
			ToLua.Push(L, LuaInterface.LuaEngine.Master);
			return 1;
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int get_State(IntPtr L)
	{
		object o = null;

		try
		{
			o = ToLua.ToObject(L, 1);
			LuaInterface.LuaEngine obj = (LuaInterface.LuaEngine)o;
			LuaInterface.LuaState ret = obj.State;
			ToLua.PushObject(L, ret);
			return 1;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, o, "attempt to index State on a nil value");
		}
	}
}
