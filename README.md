# lun-framework框架Examples工程

项目主要列出`lun-framework`框架功能示例。

## 注意：项目依赖`lun-framework-client`，拉取项目后需要更新子模块。

# lun-framework框架

`lun-framework`是基于tolua的Unity热更新框架，通过使用Lua脚本组件替代C#脚本组件的方式实现热更新。因此使用此框架在开发流程上不会有改变，非常适合于习惯C#脚本组件开发流程的开发者使用此框架实现游戏的热更新功能。同样也可以把完成后的游戏通过替换脚本的方式渐进实现热更新，如此不用把所有C#脚本替换为Lua脚本就可以一步一步调试运行，避免代码修改过多调试困难。

[lun-framework框架开源仓库](https://gitee.com/nomat/lun-framework-client)
